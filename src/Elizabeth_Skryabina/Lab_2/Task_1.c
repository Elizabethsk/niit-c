/* Bomb
*/
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <windows.h>

int main()
{
	int t = 0;
	float h = 0;
	const float g = 9.81f;
	printf("Enter height - ");
	scanf("%f", &h);
	do
	{
		printf("t=%2d c  h=%06.1f m\n", t, h);
		t++;
		h -= (g*t / 2);
		Sleep(1000);
	} while (h>0);
	printf("BABAH!!!\n");
	return 0;
}