/* Generation 10 passwords
*/
#define _CRT_SECURE_NO_WARNINGS
#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#define NUMPASS 10
#define NUMSimvPass 8

int main()
{
	int i, j;
	char simv[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghigklmnopqrstuvwxyz0123456789";
	char parol[NUMSimvPass][NUMPASS];
	srand(time(NULL));
	printf("%d passwords:\n", NUMPASS);
	for (j = 0; j<NUMPASS; j++)
		for (i = 0; i<NUMSimvPass; i++)
			parol[i][j] = simv[rand() % (strlen(simv))];
	for (j = 0; j<NUMPASS; j++)
	{
		for (i = 0; i<NUMSimvPass; i++)
			printf("%c", parol[i][j]);
		printf("\n");
	}
	return 0;
}