#define _CRT_SECURE_NO_WARNINGS
#include "Task_2_Keywords.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int max = 0;

struct NODE * makeTree(struct NODE *root, char *word) //����������� ������
{
	if (root == NULL) //���� ��������� � ������ ��������, �� ...
	{
		struct NODE *temp = (struct NODE*)malloc(sizeof(struct NODE)); //�������� ������ ��� ����� ���������
		strcpy(temp->word, word);//�������� ����� ������ ����
		temp->count = 0;
		temp->left = temp->right = NULL; //�������� ���� � ���� ��������, �.�. ���� �����
		return temp; //���������� ����� ���� � �������� � ���������� � ������������ ���������
	}
	else if (strcmp(root->word, word)>0) //���������� ������ � ����������� ������ (��� ���������� zoo)
	{
		root->left = makeTree(root->left, word);
		root->count = 0;
		return root;
	}
	else if (strcmp(root->word, word)<0) //���������� ������ � ����������� ������
	{
		root->right = makeTree(root->right, word); //�������� �� �������� �-�, �� ��� � ����� ������ ������������, �� ��� �� ������������� ��-�� (�������������� �������� 0 � ������� � ������ �-�� ����� �����������)
		root->count = 0; 
		return root;
	}
}

void printTree(struct NODE *root) //������ ������ ������������ �������
{
	if (root)//���� ���� �� ������, �� ���� �����
	{
		printTree(root->left);
		printf("%s - %d\n", root->word,root->count );
		printTree(root->right);
	}
}

struct NODE searchTree(struct NODE *root, char * buf) //����� � ������ ������� ����������
{
	if (root)
	{
		searchTree(root->left, buf);
		if (strcmp(root->word, buf) == 0)
			root->count++;
		searchTree(root->right, buf);
	}
}

int search_max(struct NODE *root)
{
	if (root)
	{
		search_max(root->left);
		if (max < root->count)
			max = root->count;
		search_max(root->right);
	}
	return max;
}

int printTree_sort(struct NODE *root, int maxcount)
{
	if (root)
	{
		printTree_sort(root->left,maxcount);
		if (maxcount == root->count)
			printf(" %d - %s\n", root->count, root->word);
		printTree_sort(root->right, maxcount);
	}
	return 0;
}