#define _CRT_SECURE_NO_WARNINGS
#include "Task_1_regioncode.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <Windows.h>

int main()
{
	FILE *fp;
	int count = 0;
	char f;
	char buf[512];
	PITEM head, tail, item;
	fp = fopen("fips10_4.csv", "rt");
	if (!fp)
	{
		perror("File: ");
		exit(1);
	}
	fgets(buf, 512, fp);
	while (fgets(buf, 512, fp))
	{
		if (count == 0)
		{
			head = createList(createStuctRegion(buf));
			tail = head;
		}
		else
			tail = addToTail(tail, createStuctRegion(buf));
		count++;
	}
	fclose(fp);
	while (1)
	{
		printf("How do you want to search information?\nIf you want to search by region write - R, by country - C: ");
		scanf("%c", &f);
		if (f != 'C' && f != 'c' && f != 'r' && f != 'R')
		{
			printf("Try again!\n\n");
			cleancash();
		}
		else
			break;
	}
	system("cls");
	if (f == 'C' || f == 'c')
	{
		printf("Enter short country's name: ");
		scanf("%s", &buf);
		findByCountry(head, buf, tail);
	}
	else
	{
		printf("Enter region: ");
		scanf("%s", &buf);
		item = findByRegion(head, buf);
	}
	return 0;
}