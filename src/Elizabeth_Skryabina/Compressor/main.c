/* Help:
	write:  (file.exe's name) -c (file's name for arhiving)
		(file.exe's name) -d (file's name for decompression)
*/
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Compressor.h"

int main(int argc, char *argv[])
{
	if (argc < 2)
	{
		printf("No files!\n");
		return -1;
	}
	if (strcmp(argv[1], "-c") == 0)
		Compressor(argv[2]);
	if (strcmp(argv[1], "-d") == 0)
		Decompressor(argv[2]);
	return 0;
}