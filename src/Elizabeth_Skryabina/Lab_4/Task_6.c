/*Enter the number of family members*/
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#define N 256
#define M 256

int main()
{
	char relative[N][M] = { 0 };
	char *young, *old;
	int youngAGE, oldAGE, age, NumRelatives,i;
	printf("Enter the number of family members: ");
	scanf("%d", &NumRelatives);

	for (i = 0; i < NumRelatives; i++)
	{
		printf("Enter name and age %d relative(e.g. name age): ", i);
		scanf("%s%d", &relative[i],&age);
		if (i == 0)
		{
			youngAGE = age;
			oldAGE = age;
		}
		if (age < youngAGE)
		{
			youngAGE = age;
			young = relative[i];
		}
		if (age > oldAGE)
		{
			oldAGE = age;
			old = relative[i];
		}
	}
	i = 0;
	printf("The youngest - ");
	while(young[i]!='\0')
		printf("%c",young[i++]);
	i = 0;
	printf("\nThe oldest - ");
	while (old[i] != '\0')
		printf("%c", old[i++]);
	puts("");
	return 0;
}