#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>

#define N 100
#define M 80

int main()
{
	FILE *fp;
	char Text[N][M] = { 0 }; 
	char *pstr[N] = { 0 }, *p;
	int i = 0, j, numStr=0, index, nminstr;
	fp = fopen("Task_5_read.txt", "rt");
	if (fp == NULL)
	{
		perror("File:");
		return 1;
	}
	char ch;
	while (i < N && (ch = fgetc(fp)) != EOF)
	{
		numStr++;
		Text[i][0] = ch;
		j = 1;
		while((Text[i][j] = fgetc(fp)) != '\n' && Text[i][j]!=EOF)
			j++;
		if (Text[i][j] == EOF)
		{
			Text[i][j] = '\n';
			break;
		}
		i++;
	}
	printf("");
	fclose(fp);
	i = 0;
	while (i < numStr)
		pstr[i] = Text[i++];
	for (i = 0; i < numStr - 1; i++)
	{
		index = i;
		nminstr = 0;
		p = pstr[i];
		for (j = i + 1; j < numStr; j++)
			if (strlen(pstr[j]) < strlen(p))
			{
				p = pstr[j];
				nminstr = j;
			}
		if (nminstr != 0)
		{
			pstr[nminstr] = pstr[i];
			pstr[i] = p;
		}
	}

	fp = fopen("Task_5_out.txt", "wt");
	if (fp == NULL)
	{
		perror("File:");
		return 1;
	}
	i = 0;
	while (i < numStr)
		fputs(pstr[i++], fp);
	fclose(fp);
	return 0;
}