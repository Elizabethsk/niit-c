/*search max word
*/
#include <stdio.h>
#include <string.h>
#define N 256

void EnterText(char str[]);
void SearchMaxWord(char str[]);
void PrintMaxWord();
char *pmax;

int main()
{
	char str[N] = { 0 };
	EnterText(str);
	SearchMaxWord(str);
	PrintMaxWord();
	return 0;
}

void EnterText(char str[])
{
	printf("Enter your text:\n");
	fgets(str, N, stdin);
	str[strlen(str) - 1] = 0;
}

void SearchMaxWord(char str[])
{
	pmax = str;
	int i = 0, maxlength = 0, wordlength = 0, inWord = 0;
	while (str[i])
	{
		while (str[i] != ' ' && str[i] != '\0')
		{
			wordlength++;
			i++;
		}
		if (wordlength > maxlength)
		{
			maxlength = wordlength;
			pmax = &str[i - wordlength];
		}
		wordlength = 0;
		i++;
	}
}

void PrintMaxWord()
{
	printf("Max word: ");
	while ((*pmax != ' ') && (*pmax != '\0'))
		putchar(*pmax++);
	puts("");
}