#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <windows.h>
#define N 37
#define M 99

void cleanFild();
void fillingFild();
void copyFild();
void printFild();
char fild[N][M];

int main()
{
	srand(time(NULL));
	while (1)
	{
		cleanFild();
		fillingFild();
		copyFild();
		system("cls");
		printFild();
		Sleep(2000);
	}
	return 0;
}

void cleanFild()
{
	int i, j;
	for (i = 0; i < N; i++)
		for (j = 0; j < M; j++)
			fild[i][j] = ' ';
	return 0;
}

void fillingFild()
{
	int i = 0, line, column, max;
	max = (M*N)/8;
	while (i < max)
	{
		line = rand() % N / 2;
		column = rand() % M / 2;
		if (fild[line][column] != '*')
		{
			fild[line][column] = '*';
			i++;
		}
	}
	return 0;
}

void copyFild()
{
	int i, j;
	for (i = 0; i < (N / 2)+1; i++)
		for (j = 0; j < M / 2; j++)
			fild[i][M - 1 - j] = fild[i][j];
	for (j = 0; j < M; j++)
		for (i = 0; i < N / 2; i++)
			fild[N - 1 - i][j] = fild[i][j];
	return 0;
}

void printFild()
{
	int i, j;
	for (i = 0; i < N; i++)
	{
		for (j = 0; j < M; j++)
			printf("%c", fild[i][j]);
		putchar('\n');
	}
	return 0;
}