/*print random words
*/
#include <stdio.h>
#include <string.h>
#include <time.h>
#define N 256

void EnterString(char *str);
int GetWords(char *str, char **pwords);
void PrintWord(char **pwords, int count);

int main()
{
	char str[N] = { 0 };
	char *pwords[N];
	int count = 0, numRandWord;
	srand(time(NULL));
	EnterString(str);
	count = GetWords(str, pwords);
	while (count > 0)
	{
		numRandWord = rand() % count;
		PrintWord(pwords, numRandWord);
		pwords[numRandWord] = pwords[count-1];
		count--;
	}
	putchar('\n');
	return 0;
}

void EnterString(char *str)
{
	printf("Enter your text:\n");
	fgets(str, 256, stdin);
	str[strlen(str) - 1] = 0;
}

int GetWords(char *str, char **pwords)
{
	int count = 0, inWord = 0, i = 0;
	while (str[i])
	{
		if (str[i] != ' ' && inWord == 0)
		{
			inWord = 1;
			pwords[count++] = str + i;
		}
		else
			if (str[i] == ' ' && inWord == 1)
				inWord = 0;
		i++;
	}
	return count;
}

void PrintWord(char **pwords, int numRandWord)
{
	char *p;
	{
		p = pwords[numRandWord];
		while ((*p != ' ') && (*p != '\0'))
			putchar(*p++);
		putchar(' ');
	}
}