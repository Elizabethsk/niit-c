#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

void cleancash()
{
	int c;
	do {
		c = getchar();
	} while (c != '\n' && c != EOF);
}

int main()
{
	int h, m, s;
	while (1)
	{
		printf("Enter the time in the format hh:mm:ss ");
		if (scanf("%d:%d:%d", &h, &m, &s) != 3)
		{
			printf("Not correct format!\n");
			cleancash();
		}
		else
		{
			if (h<0 || h >= 24 || m<0 || m >= 60 || s<0 || s >= 60)
			{
				printf("Not correct time!\n");
				cleancash();
			}
			else
				break;
		}
	}
	if (h >= 3 && h<9)
		printf("Good morning\n");
	if (h >= 9 && h<15)
		printf("Good day\n");
	if (h >= 15 && h<21)
		printf("Good evening\n");
	if (h >= 21 || h<3)
		printf("Good night\n");
	return 0;
}