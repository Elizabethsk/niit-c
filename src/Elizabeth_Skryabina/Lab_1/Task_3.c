#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

int main()
{
	const float p = 3.1416f;
	float enterang, rezultang;
	char c;
	while (1)
	{
		printf("Enter the angle in degrees (format: xD) or radians (format: xR), x-number - ");
		scanf("%f%c", &enterang, &c);
		if (c == 'D')
		{
			rezultang = enterang*p / 180;
			break;
		}
		else {
			if (c == 'R')
			{
				rezultang = enterang * 180 / p;
				break;
			}
			else {
				printf("Not correct format!\n");
				do {
					c = getchar();
				} while (c != '\n' && c != EOF);
			}
		}
	}
	if (c == 'D')
		printf("%6.2f %c = %6.2f R\n", enterang, c, rezultang);
	else
		printf("%6.2f %c = %6.2f D\n", enterang, c, rezultang);
	return 0;
}