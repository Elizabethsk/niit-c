/* 6.3 Transformation of int to char
*/
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

char array[100] = { 0 };
int kolvo = 0;

void Transformation(int n);
void PrintArray();

int main()
{
	int number, i = 0;
	printf("Enter number: ");
	scanf("%d", &number);
	printf("symbols: ");
	Transformation(number);
	PrintArray();
	return 0;
}

void Transformation(int n)
{
	int i;
	if (n < 0)
	{
		array[kolvo++] = '-';
		n = -n;
	}
	if ((i = n / 10) != 0)
		Transformation(i);
	array[kolvo++] = n % 10 + '0';
}

void PrintArray()
{
	int i = 0;
	while (array[i] != '\0')
		printf("%c", array[i++]);
	putchar('\n');
}