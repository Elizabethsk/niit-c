/* 6.7 Labyrinth
*/
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

#define N 9
#define M 28

enum Boolean {false,true} flag;

char Labyrinth[N][M] = {
	"############################",
	"#           #   #          #",
	"##########  #   #          #",
	"#           #   #######  ###",
	"# ######    # x            #",
	"#      #    #   #######   ##",
	"#####  #    #   #         ##",
	"       #        #     ######",
	"############################",
};

void SearchStartAndExit();
void SearchExit(int i, int j);
void printLabyrinth();

int main()
{
	printLabyrinth();
	SearchStartAndExit();
	printLabyrinth();
	printf("Congratulations! You came out of the labyrinth\n");
	return 0;
}

void SearchStartAndExit()
{
	int i = 1, j = 1, StartPosition_i, StartPosition_j;
	while (i < N)
	{
		j = 1;
		while (j < M)
		{
			if (Labyrinth[i][j] == 'x')
			{
				StartPosition_i = i;
				StartPosition_j = j;
				SearchExit(StartPosition_i, StartPosition_j);
				return 0;
			}
			j++;
		}
		i++;
	}
}

void SearchExit(int i, int j)
{
	if (i != 0 && i != N && j != M && j != 0)
	{
		Labyrinth[i][j] = '.';
		if (Labyrinth[i - 1][j] == ' ' && flag == false)
			SearchExit(i - 1, j);
		if (Labyrinth[i + 1][j] == ' ' && flag == false)
			SearchExit(i + 1, j);
		if (Labyrinth[i][j + 1] == ' ' && flag == false)
			SearchExit(i, j + 1);
		if (Labyrinth[i][j - 1] == ' ' && flag == false)
			SearchExit(i, j - 1);
	}
	else
		flag = true;
	Labyrinth[i][j] = '.';
}

void printLabyrinth()
{
	int i, j;
	for (i = 0; i < N; i++)
	{
		for (j = 0; j < M; j++)
			putchar(Labyrinth[i][j]);
		putchar('\n');
	}
	putchar('\n');
}