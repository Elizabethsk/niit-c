/* 6.8 Calculator
*/
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define N 256

int —heckErrors(char *buf);
char partition(char *buf, char *expr1, char *expr2);
void DelBracket(char *buf);
int eval(char *buf);

int main(int argc, char* argv[])
{
	int result, ch;
	if (argc<2)
	{
		puts("Error. No data.");
		return 1;
	}
	if (ch = —heckErrors(argv[1]) == 0)
		return 1;
	result = eval(argv[1]);
	printf("Result = %d\n", result);
	return 0;
}

int —heckErrors(char *buf)
{
	enum Boolean { false, true } flag = true;
	int i = 0;
	while (buf[i])
	{
		if ((buf[i] >= '0' && buf[i] <= '9') || buf[i] == '+' || buf[i] == '-' || buf[i] == '*' || buf[i] == '/' || buf[i] == '(' || buf[i] == ')')
			flag = true;
		else {
			flag = false;
			break;
		}
		i++;
	}
	if (flag == false)
		printf("Error!\n");
	return flag;
}

char partition(char *buf, char *expr1, char *expr2)
{
	int open = 0, close = 0, i = 0, j, m;
	char operation;
	while (buf)
	{
		if (buf[i] == '(')
			open++;
		if (buf[i] == ')')
			close++;
		if (open == close)
			break;
		i++;
	}
	for (j = 0; j <= i; j++)
		expr1[j] = buf[j];
	expr1[j] = '\0';
	operation = buf[i + 1];
	for (j = i + 2, m = 0; buf[j] != '\0'; j++, m++)
		expr2[m] = buf[j];
	expr2[m] = '\0';
	return operation;
}

void DelBracket(char *buf)
{
	int len = strlen(buf);
	buf[len - 1] = '\0';
	int i;
	for (i = 1; i < len; i++)
		buf[i - 1] = buf[i];
}

int eval(char *buf)
{
	char expr1[N], expr2[N], operation;
	if (*buf == '(')
	{
		DelBracket(buf);
		operation = partition(buf, expr1, expr2);
		switch (operation)
		{
		case '+':
			return eval(expr1) + eval(expr2);
		case '-':
			return eval(expr1) - eval(expr2);
		case '/':
			return eval(expr1) / eval(expr2);
		case '*':
			return eval(expr1) * eval(expr2);
		}
	}
	else
		return atoi(buf);
}