/* 6.5 Fibonacci
*/
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <time.h>
#define N 40

long long Fibonacci(int n);

int main()
{
	FILE * fp;
	clock_t start, end;
	int i;
	long long fib;
	fp = fopen("Task_5_outFibonacci.txt", "wt");
	if (fp == NULL)
	{
		perror("File:");
		return 1;
	}
	for (i = 1; i <= N; i++)
	{
		start = clock();
		fib = Fibonacci(i);
		end = clock();
		printf("%2d %10lld %10f\n", i, fib, (double)(end - start) / CLOCKS_PER_SEC);
		fprintf(fp, "%d	%lld	%f\n", i, fib, (double)(end - start) / CLOCKS_PER_SEC);
	}
	fclose(fp);
	return 0;
}

long long Fibonacci(int n)
{
	if (n <= 2)
		return 1;
	else
		return Fibonacci(n - 1) + Fibonacci(n - 2);
}