#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>

int countnum(char *str);
void cleancash();
int enternumword(numallWords);

int main()
{
	char str[256] = { 0 };
	char *pdelword = str, *pnextword = str;
	int numWord, numallWords, i = 0, inWord = 0;
	printf("Enter your text (few words):\n");
	fgets(str, 256, stdin);
	str[strlen(str) - 1] = 0;
	numallWords = countnum(str);
	numWord = enternumword(numallWords);
	//Search word for deletion
	while (numWord > 0)  
	{
		if (str[i] != ' ' && inWord == 0)
		{
			inWord = 1;
			numWord--;
			pdelword = &str[i];
		}
		else
			if (str[i] == ' ' && inWord == 1)
				inWord = 0;
		i++;
	}
	//Search next word
	pnextword = pdelword;
	while (*pnextword != ' ' && *pnextword != '\0')
		pnextword++;
	//Delete
	while (*pdelword != '\0')
	{
		*pdelword = *pnextword;
		pdelword++;
		pnextword++;
	}
	printf("\nNew text:\n%s\n", str);
	return 0;
}

int countnum(char *str)
{
	int numwords = 0, i = 0;
	if (str[0] != ' ')
		numwords++;
	while (str[i] != '\0')
	{
		if ((str[i] == ' ') && (str[i + 1] != ' ') && (str[i + 1] != '\0'))
			numwords++;
		i++;
	}
	return numwords;
}

void cleancash()
{
	int c;
	do {
		c = getchar();
	} while (c != '\n' && c != EOF);
}

int enternumword(numallWords)
{
	int numWord;
	while (1)
	{
		printf("Enter the number words that you want to print :  ");
		scanf("%d", &numWord);
		if (numWord > numallWords)
		{
			printf("Error! You don't have %d words! Try again!\n", numWord);
			cleancash();
		}
		else
			break;
	}
	return numWord;
}