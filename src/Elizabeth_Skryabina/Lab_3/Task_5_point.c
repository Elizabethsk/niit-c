/*Sum between first negative and last positive numbers
*/
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#define MAX 10

void createarray(int *numb);

int main()
{
	int numb[MAX] = { 0 };
	int *negativeNum = numb, *positiveNum, sum = 0;
	srand(time(NULL));
	createarray(numb);
	positiveNum = &numb[MAX-1];
	while (*negativeNum >= 0)
		negativeNum++;
	printf("\nFirst negative: %d\n", *negativeNum);
	while (*positiveNum < 0)
		positiveNum--;
	printf("Last positive: %d\n", *positiveNum);
	if (negativeNum < positiveNum)
		for (positiveNum--; positiveNum > negativeNum; positiveNum--)
			sum += *positiveNum;
	else
		for (negativeNum--; positiveNum < negativeNum; negativeNum--)
			sum += *negativeNum;

	printf("\nsumm = %d\n", sum);
	return 0;
}

void createarray(int *numb)
{
	int i;
	for (i = 0; i<MAX; i++)
	{
		numb[i] = rand() % 20 - 10;
		printf("%d  ", numb[i]);
	}
}