/*Sum between minimum and maximum numbers (latest)
*/
#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#define MAX 10

void createarray(int *numb);

int main()
{
	int numb[MAX] = { 0 };
	int i, sum = 0, *pmin = numb, *pmax = numb;
	srand(time(NULL));
	createarray(numb);
	for (i = 1; i<MAX; i++)
	{
		if (numb[i] <= *pmin)
			pmin = &numb[i];
		if (numb[i] >= *pmax)
			pmax = &numb[i];
	}
	printf("\nmin = %d\n", *pmin);
	printf("max = %d\n", *pmax);
	if (pmin<pmax)
		for (++pmin; pmin < pmax; pmin++)
			sum += *pmin;
	else
		for (++pmax; pmax < pmin; pmax++)
			sum += *pmax;
	printf("sum = %d\n", sum);
	return 0;
}

void createarray(int *numb)
{
	int i;
	for (i = 0; i<MAX; i++)
	{
		numb[i] = rand() % 100 - 50;
		printf("%d  ", numb[i]);
	}
	printf("\n");
}