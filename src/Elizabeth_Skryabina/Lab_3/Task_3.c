/*search max word
*/
#include <stdio.h>
#include <string.h>

int main()
{
	char str[256] = { 0 };
	char *pmax = str;
	int i = 0, maxlength = 0, wordlength = 0;
	printf("Enter your text:\n");
	fgets(str, 256, stdin);
	str[strlen(str) - 1] = 0;
	if (str[i] != ' ')
		while (str[i] != ' '&&str[i++])
			maxlength++;
	while (str[i])
	{
		if (str[i] == ' '&&str[i] != '\0'&&str[++i] != ' '&&str[i] != '\0')
		{
			wordlength = 0;
			while (str[i] != ' '&&str[i] != '\0')
			{
				wordlength++;
				i++;
			}
		}
		if (wordlength>maxlength)
		{
			maxlength = wordlength;
			pmax = &str[i - maxlength];
		}
	}
	printf("Max word: ");
	while ((*pmax != ' ') && (*pmax != '\0'))
		putchar(*pmax++);
	printf("\n%d letters\n", maxlength);
	return 0;
}