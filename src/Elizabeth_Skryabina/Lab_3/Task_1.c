/* count the number of words
*/
#include <stdio.h>
#include <string.h>

int countnum(char *str);

int main()
{
	char str[256] = { 0 };
	printf("Enter your text:\n");
	fgets(str, 256, stdin);
	str[strlen(str) - 1] = '\0';
	countnum(str);
	return 0;
}

int countnum(char *str)
{
	int numwords = 0, i = 0;
	if (str[0] != ' ')
		numwords++;
	while (str[i] != '\0')
	{
		if ((str[i] == ' ') && (str[i + 1] != ' ') && (str[i + 1] != '\0'))
			numwords++;
		i++;
	}
	printf("in the text %d words\n", numwords);
	return 0;
}
